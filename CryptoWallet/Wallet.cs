﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CryptoWallet {
    public class Wallet {
        public Wallet() {
            Currencies = new List<CryptoCurrency>();
        }

        public void AddCurrency(CryptoCurrency c) {
            Currencies.Add(c);
        }

        public static void SnimiUXml(object o, string fileName) {
            XmlSerializer xmlFormat = new XmlSerializer(o.GetType());
            using (Stream fs = new FileStream(fileName,
              FileMode.Create, FileAccess.Write, FileShare.None)) {
                xmlFormat.Serialize(fs, o);
            }
        }

        public static object UcitajIzXml(string fileName) {
            XmlSerializer xmlFormat = new XmlSerializer(typeof(Wallet));
            using (Stream fStream = File.OpenRead(fileName)) {
                return xmlFormat.Deserialize(fStream);
            }
        }

        public List<CryptoCurrency> Currencies;
    }
}
