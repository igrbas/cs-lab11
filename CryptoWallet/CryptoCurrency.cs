﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWallet {
    public class CryptoCurrency {

        public CryptoCurrency() {
            this.name = "";
            this.amount = 0;
            this.value = 0;
        }
        public CryptoCurrency(String name, double amount, double value) {
            this.name = name;
            this.amount = amount;
            this.value = value;
        }

        public override string ToString() {
            return String.Format("{0} - {1} - {2}$", name, amount, value);
        }

        public String name;
        public double amount;
        public double value;
    }
}
