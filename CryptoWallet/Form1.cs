﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CryptoWallet {
    public partial class Form1 : Form {
       Wallet wallet;
        public Form1() {
            InitializeComponent();
            wallet = new Wallet();
            try {
                wallet = (Wallet)Wallet.UcitajIzXml(@"./save.xml");
            } catch (Exception e) {}
            ShowCurrency();
        }

        private void btnSave_Click(object sender, EventArgs e) {
            wallet.AddCurrency(new CryptoCurrency(tbName.Text, Double.Parse(tbAmount.Text), Double.Parse(tbValue.Text)));
            ShowCurrency();
            Wallet.SnimiUXml(wallet, @"./save.xml");
        }

        private void ShowCurrency() {
            lbCurrencies.Items.Clear();
            foreach(var c in wallet.Currencies) {
                lbCurrencies.Items.Add(c);
            }
        }

        private void lbCurrencies_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}
